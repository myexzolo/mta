<?php
  date_default_timezone_set('Asia/Bangkok');

  use PHPMailer\PHPMailer\PHPMailer;

  if(isset($_POST['name']) && isset($_POST['email']))
  {
      $name     = $_POST['name'];
      $email    = $_POST['email'];
      $subject  = $_POST['subject'];
      $message  = $_POST['message'];

      require_once "../PHPMailer/PHPMailer.php";
      require_once "../PHPMailer/SMTP.php";
      require_once "../PHPMailer/Exception.php";

      // $receiving_email_address = 'myexzolo@hotmail.com';
      $receiving_email_address = 'mta.service@hotmail.com';

      $mail = new PHPMailer();

      $mail->isSMTP();
      $mail->CharSet = "UTF-8";
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPAuth = true;
      $mail->Username = 'info.mtaservice@gmail.com';
      $mail->Password = 'adminMTAPass';
      $mail->Port = 465;
      $mail->SMTPSecure = "ssl";

      $mail->isHTML(true);
      $mail->setFrom($email, "MTA service มีผู้ติดต่อเรา");
      $mail->addAddress($receiving_email_address);
      $mail->Subject = $subject;
      $mail->Body = $message."<br> ผู้ติดต่อ : ".$name."<br> mail : ".$email;

      if($mail->send())
      {
        $status = "success";
        $response = "Email is sent";
      }else
      {
        $status = "danger";
        $response = "Something is wong " . $mail->ErrorInfo;
      }

      exit(json_encode(array("status" => $status, "response" => $response)));
  }
?>
