<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>MTA Service - Index</title>

  <?php include('inc/css.php'); ?>
</head>

<body onload="getButton();loadpageConcact()">

  <?php
    include('inc/connect.php');
    include('inc/mainFunc.php');
    include('inc/nav.php');
    include('page/banner.php');
  ?>

  <main id="main">
    <div id="product"></div>
    <?php
        include('page/product.php');
        include('page/service.php');
        include('page/aboutus.php');
        // include('page/concact.php');
    ?>
    <div id="concact"></div>

  </main><!-- End #main -->

  <?php
    include('inc/footer.php');
    include('inc/js.php');
  ?>

</body>
</html>
<script>
// $.get("page/product.php")
// .done(function(data) {
//   $("#product").html(data);
// });

function loadpageConcact(){
  $.get("page/concact.php")
  .done(function(data) {
    $("#concact").html(data);
  });
}


function sendEmail() {
  var name  = $("#name");
  var email = $("#email");
  var subject = $("#subject");
  var message = $("#message");
  if (isNotEmpty(name) && isNotEmpty(email) && isNotEmpty(subject))
  {
    $.ajax({
        url: 'forms/contact.php',
        type: 'POST',
        data: {
          name : name.val(),
          email : email.val(),
          subject : subject.val(),
          message : message.val()
        },
        dataType: 'json'
    }).done(function( data ) {
        $('#formMail')[0].reset();
        $.smkAlert({text: data.response,type: data.status});
    });
  }
}

function isNotEmpty(caller)
{
  if(caller.val() == "")
  {
    caller.css('border', '1px solid red');
    return false;
  }else
  {
    caller.css('border', '');
    return true;
  }
}
</script>
