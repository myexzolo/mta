<?php
    session_start();
    include("../../../inc/function/mainFunc.php");
    include("../../../inc/function/connect.php");

    $action       = $_POST["action"];
    $id           = $_POST["id"];
    $_POST['user_id'] = $_SESSION['member'][0]['user_id'];

    // --ADD EDIT DELETE Module-- //
    if(empty($id) && $action == "ADD"){
      // to do some thing
      unset($_POST["action"]);
      unset($_POST["id"]);
      unset($_POST["b_id"]);
      $_POST['b_code'] = strtoupper($_POST['b_code']);
      $sql = DBInsertPOST($_POST,'brand');


    }else if($action == "EDIT"){
      // to do some
      unset($_POST["action"]);
      unset($_POST["id"]);
      $_POST['b_code'] = strtoupper($_POST['b_code']);
      $sql = DBUpdatePOST($_POST,'brand','b_id');

    }else{
      // to do some thing
      $_POST['is_active'] = 'D';
      $_POST['b_id'] = $_POST["id"];
      unset($_POST["action"]);
      unset($_POST["id"]);
      $sql = DBUpdatePOST($_POST,'brand','b_id');

    }

    $query      = DbQuery($sql,null);
    $row        = json_decode($query, true);
    $errorInfo  = $row['errorInfo'];

    if(intval($row['errorInfo'][0]) == 0){
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'success','message' => 'Success')));
    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'danger','message' => 'Fail')));
    }
    
  ?>
