<?php
    session_start();
    include("../../../inc/function/connect.php");
    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);

    $action = $_POST["value"];
    $id = $_POST["id"];

    if($action == "EDIT"){
      $btn = "Update changes";

      $sql = "SELECT * FROM brand WHERE b_id = '$id'";
      $query = DbQuery($sql,null);
      $row   = json_decode($query, true)['data'][0];

      $b_id   = $row['b_id'];
      $b_code = $row['b_code'];
      $b_name = $row['b_name'];
      $b_seq  = $row['b_seq'];
      $is_active  = $row['is_active'];

    }
    if($action == "ADD"){
     $btn = "Save changes";
    }
    ?>
    <input type="hidden" name="action" value="<?=$action?>">
    <input type="hidden" name="id" value="<?=@$id?>">
    <input type="hidden" name="b_id" value="<?=@$id?>">
    <div class="modal-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>BRAND CODE</label>
            <input value="<?=@$b_code?>" name="b_code" type="text" class="form-control text-uppercase" placeholder="BRAND CODE" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>BRAND NAME</label>
            <input value="<?=@$b_name?>" name="b_name" type="text" class="form-control" placeholder="BRAND NAME" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>SEQ</label>
            <input value="<?=@$b_seq?>" min="1" name="b_seq" type="number" class="form-control" placeholder="SEQ" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>STATUS</label>
            <select class="form-control" name="is_active" required>
              <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
              <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
    </div>
