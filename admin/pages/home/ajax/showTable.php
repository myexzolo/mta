<?php
  session_start();
  include('../../../inc/function/mainFunc.php');
  include('../../../inc/function/connect.php');
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);


?>

<div class="table-responsive">
  <table class="table table-bordered" id="dataTable" width="100%">
    <thead>
      <tr>
        <th>No.</th>
        <th>Shortname.</th>
        <th>Categoryid.</th>
        <th>Fullname.</th>
        <th>Displayname.</th>
        <th>Date Create.</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>No.</th>
        <th>Shortname.</th>
        <th>Categoryid.</th>
        <th>Fullname.</th>
        <th>Displayname.</th>
        <th>Date Create.</th>
      </tr>
    </tbody>
  </table>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#dataTable').DataTable();
  });
</script>
