<?php
  session_start();
  include("../../../inc/function/connect.php");
  include("../../../inc/function/mainFunc.php");
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  ?>
  <style>
    th {
      text-align: center;
      background-color: #ebebeb;
    }
  </style>
  <table class="table table-bordered table-striped" id="tableDisplay">
    <thead>
      <tr class="text-center">
        <th>No</th>
        <th>รูปถาพ</th>
        <th>BRAND</th>
        <th style="width:400px;">CATEGORY</th>
        <th>ชื่อสินค้า</th>
        <th>ลำดับการแสดง</th>
        <th>แสดงหน้าหลัก</th>
        <th>SEQ แสดงหน้าหลัก</th>
        <th>LAST UPDATE</th>
        <th>สถานะ</th>
        <th>Edit</th>
        <th>Del</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $sql = "SELECT * FROM product WHERE 1=1 AND is_active != 'D' ORDER BY p_seq DESC";
        $query = DbQuery($sql,null);
        $json   = json_decode($query, true);
        if($json['dataCount'] > 0){
          foreach ($json['data'] as $key => $value) {
      ?>
      <tr class="text-center">
        <td><?=$key+1?></td>
        <td>
          <img width="50px" src="../../../image/product/<?=$value['p_img']?>" alt="">
        </td>
        <td >
          <?php
            $sqlb = "SELECT b_name FROM brand WHERE b_id IN({$value['b_id']})";
            $queryb = DbQuery($sqlb,null);
            $jsonb   = json_decode($queryb, true);
              $new_arr = array();
              foreach ($jsonb['data'] as $valueb) {
                $new_arr[] = $valueb['b_name'];
              }
              echo implode(",",$new_arr);
          ?>
        </td>
        <td align="left">
          <?php
            $sqlb = "SELECT c_name FROM category WHERE c_id IN({$value['c_id']})";
            $queryb = DbQuery($sqlb,null);
            $jsonb   = json_decode($queryb, true);
            $countData = $jsonb['dataCount'];
            $rows     = $jsonb['data'];
             $new_arr = array();
              foreach ($jsonb['data'] as $valueb) {
                $new_arr[] = $valueb['c_name'];
              }
              echo implode(", ",$new_arr);
          ?></td>
        <td align="left"><?=$value['p_name']?></td>
        <td ><?=$value['p_seq']?></td>
        <td><?=$value['is_show']=='Y'?"แสดง":"ไม่แสดง";?></td>
        <td ><?=$value['p_seq_show']?></td>
        <td><?=DateTimeThai($value['date_update'])?></td>
        <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
        <td>
          <a class="btn_point"><i class="fa fa-edit" style="font-size:20px;"  onclick="showForm('EDIT','<?=$value['p_id']?>')"></i></a>
        </td>
        <td>
          <a class="btn_point text-danger"><i class="fa fa-trash-o" style="font-size:20px;" onclick="delModule('<?=$value['p_id']?>')"></i></a>
        </td>
      </tr>
      <?php }} ?>
    </tbody>
  </table>
  <script>
    $(function () {
      $('#tableDisplay').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
