<?php
    session_start();
    include("../../../inc/function/connect.php");
    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);

    $action = $_POST["value"];
    $id = $_POST["id"];
    $required = 'required';
    $strc_id = '';
    if($action == "EDIT"){
      $required = '';
      $btn = "Update changes";
      $sql = "SELECT * FROM product WHERE p_id = '$id'";
      $query = DbQuery($sql,null);
      $row   = json_decode($query, true)['data'][0];

      $p_id = $row['p_id'];
      $p_name = $row['p_name'];
      $p_img = $row['p_img'];
      $p_detail = $row['p_detail'];
      $p_detail2 = $row['p_detail2'];
      $p_imgSlide = array_diff(explode("[:]", $row['p_imgSlide']),array());
      $category = explode(",",$row['c_id']);
      $p_seq = $row['p_seq'];
      $b_id = $row['b_id'];
      $is_show = $row['is_show'];
      $is_active = $row['is_active'];
      $p_seq_show = $row['p_seq_show'];

      $strc_id = "AND b_id = '$b_id'";

    }
    if($action == "ADD"){
     $btn = "Save changes";
       $strc_id = "AND b_id = '1'";
       $category = array();
    }
    ?>
    <input type="hidden" name="action" value="<?=$action?>">
    <input type="hidden" name="id" value="<?=@$id?>">
    <input type="hidden" name="p_id" value="<?=@$id?>">
    <div class="modal-body">
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>BRAND</label>
            <select class="form-control select2" name="b_id" onchange="getCat(this.value)" required>
              <?php
                $sqlb = "SELECT * FROM brand where is_active = 'Y'";
                $queryb = DbQuery($sqlb,null);
                $jsonb   = json_decode($queryb, true);
                if($jsonb['dataCount'] > 0){
                  foreach ($jsonb['data'] as $valueb) {
              ?>
              <option value="<?=$valueb['b_id']?>" <?=@$b_id==$valueb['b_id']?"selected":""?>><?=$valueb['b_name']?></option>
              <?php }} ?>
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>SEQ</label>
            <input value="<?=@$p_seq?>" name="p_seq" min="1" type="number" class="form-control" placeholder="SEQ" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>STATUS</label>
            <select class="form-control" name="is_active" required>
              <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
              <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
            </select>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>แสดงหน้าหลัก</label>
            <select class="form-control" name="is_show" required>
              <option value="N" <?=@$is_show=='N'?"selected":""?>>ไม่แสดง</option>
                <option value="Y" <?=@$is_show=='Y'?"selected":""?>>แสดง</option>
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>SEQ หน้าหลัก</label>
            <input value="<?=@$p_seq_show?>" name="p_seq_show" min="1" type="number" class="form-control" placeholder="SEQ" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>CATEGORY</label>
            <select class="form-control select2" id="c_id" name="c_id[]" required multiple>
              <?php
                $sqlc = "SELECT * FROM category WHERE 1=1 $strc_id and is_active = 'Y'";
                $queryc = DbQuery($sqlc,null);
                $jsonc   = json_decode($queryc, true);
                if($jsonc['dataCount'] > 0){
                  foreach ($jsonc['data'] as $valuec) {
              ?>
              <option value="<?=$valuec['c_id']?>" <?=in_array($valuec['c_id'], $category)?"selected":""?>><?=$valuec['c_name']?></option>
              <?php }} ?>
            </select>
          </div>
          <div class="form-group">
            <label>PRODUCT NAME</label>
            <input value="<?=@$p_name?>" name="p_name" type="text" class="form-control" placeholder="PRODUCT NAME" required>
          </div>
          <div class="form-group">
            <label>DETAIL</label>
            <textarea id="editor1"><?=@$p_detail?></textarea>
            <input type="hidden" id="p_detail" name="p_detail">
          </div>
          <div class="form-group">
            <label>DETAIL 2</label>
            <textarea id="editor2"><?=@$p_detail2?></textarea>
            <input type="hidden" id="p_detail2" name="p_detail2">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>IMAGE</label>
            <input name="p_img" onchange="readURL(this,'p_img')" type="file" class="form-control" <?=$required?>>
          </div>
          <div id="p_img">
            <?php if($action == 'EDIT'){ ?>
            <img width="100" src="../../../image/product/<?=$p_img?>">
            <?php } ?>
          </div>

          <div class="form-group">
            <label>IMAGE ETC</label>
            <input type="file" name="p_imgSlide[]" accept="image/*" class="form-control" multiple <?=$required?>>
          </div>
          <?php if($action == 'EDIT'){ ?>
            <h4><strong>IMAGE ETC</strong></h4>
            <?php foreach ($p_imgSlide as $value_slide) { ?>
              <div class="col-md-2" style="padding:0px;position: relative;">
                <img class="img-responsive" src="../../../image/product/<?=$value_slide?>">
                <label class="toggle" style="position: absolute;top: 0px;left: 0px;">
                  <input class="toggle__input" name="remove[]" value="<?=$value_slide?>" type="checkbox">
                  <span class="toggle__label">
                    <span class="toggle__text"></span>
                  </span>
                </label>
              </div>
            <?php } ?>

          <?php } ?>

        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
    </div>
    <script type="text/javascript">
      $('.select2').select2();
    </script>
    <script type="text/javascript">
      CKEDITOR.replace( 'editor1',{ height: '200px' } );
      CKEDITOR.replace( 'editor2',{ height: '200px' } );
    </script>

    <script type="text/javascript">

      function getCat(value,id){
        $.post("ajax/getCat.php",{value:value})
          .done(function( data ) {
            $("#c_id").html(data);
        });

      }

    </script>
