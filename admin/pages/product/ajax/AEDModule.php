<?php
    session_start();
    include("../../../inc/function/mainFunc.php");
    include("../../../inc/function/connect.php");

    $action       = @$_POST["action"];
    $id           = @$_POST["id"];
    $p_img        = @$_FILES['p_img'];
    $p_imgSlide        = @$_FILES['p_imgSlide'];
    $p_imgSlide2 = $p_imgSlide;
    $_POST['user_id'] = $_SESSION['member'][0]['user_id'];
    // --ADD EDIT DELETE Module-- //
    if(empty($id) && $action == "ADD"){
      // to do some thing
      unset($_POST["action"]);
      unset($_POST["id"]);
      unset($_POST["p_id"]);
      $_POST['c_id'] = @implode(",",$_POST['c_id']);
      $p_img = uploadfile($p_img,"../../../../image/product","p_");
      $_POST['p_img'] = $p_img['image'];

      $_POST['p_imgSlide'] = '';
      if($p_imgSlide['error'][0] == 0){
        $p_imgSlide = uploadfileMulti($p_imgSlide,"../../../../image/product","p_");
        $_POST['p_imgSlide'] = $p_imgSlide['image'];
      }

      $sql = DBInsertPOST($_POST,'product');

    }else if($action == "EDIT"){
      // to do some thing
      $_POST['c_id'] = @implode(",",$_POST['c_id']);
      unset($_POST["action"]);
      unset($_POST["id"]);
      if($p_img['error'] == 0){
        $sql = "SELECT p_img FROM product WHERE p_id = '{$_POST["p_id"]}'";
        $query   = DbQuery($sql,null);
        $row     = json_decode($query,true)['data'][0];
        @unlink("../../../../image/product/".$row['p_img']);
        $p_img = uploadfile($p_img,"../../../../image/product","p_");
        $_POST['p_img'] = $p_img['image'];
      }

      if(!empty($_POST['remove'])){
        $sql = "SELECT p_imgSlide FROM product WHERE p_id = '{$_POST["p_id"]}'";
        $query   = DbQuery($sql,null);
        $row     = json_decode($query,true)['data'][0];

        $p_imgSlide = array_diff(explode("[:]", $row['p_imgSlide']),array(""));
        foreach ($_POST['remove'] as $value) {
          if (($key = array_search($value, $p_imgSlide)) !== false) {
            @unlink("../../../../image/product/$value");
            unset($p_imgSlide[$key]);
          }
        }
        $str = implode("[:]",$p_imgSlide);
        $sql = "UPDATE product SET p_imgSlide = '$str' WHERE p_id = '{$_POST["p_id"]}'";
        DbQuery($sql,null);
        unset($_POST["remove"]);
      }

      if($p_imgSlide2['error'][0] == 0){
        $sql = "SELECT p_imgSlide FROM product WHERE p_id = '{$_POST["p_id"]}'";
        $query   = DbQuery($sql,null);
        $row     = json_decode($query,true)['data'][0];
        $p_imgSlide22 = array_diff(explode("[:]", $row['p_imgSlide']),array(""));
        $arr = uploadfileMulti($p_imgSlide2,"../../../../image/product","p_");
        foreach ($arr as $value) {
          array_push($p_imgSlide22,$value);
        }
        $_POST['p_imgSlide'] = implode("[:]",$p_imgSlide22);
      }

      $sql = DBUpdatePOST($_POST,'product','p_id');

    }else{
      // to do some thing

      $_POST['is_active'] = 'D';
      $_POST['p_id'] = @$_POST["id"];
      unset($_POST["action"]);
      unset($_POST["id"]);
      $sql = DBUpdatePOST($_POST,'product','p_id');


    }

    $query      = DbQuery($sql,null);
    $row        = json_decode($query, true);
    $errorInfo  = $row['errorInfo'];

    if(intval($row['errorInfo'][0]) == 0){
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'success','message' => 'Success')));
    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'danger','message' => 'Fail')));
    }

  ?>
