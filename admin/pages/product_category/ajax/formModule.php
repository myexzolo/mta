<?php
    session_start();
    include("../../../inc/function/connect.php");
    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);

    $action = $_POST["value"];
    $id = $_POST["id"];
    $b_id = array();
    if($action == "EDIT"){
      $btn = "Update changes";
      $sql = "SELECT * FROM category WHERE c_id = '$id'";
      $query = DbQuery($sql,null);
      $row   = json_decode($query, true)['data'][0];

      $c_id   = $row['c_id'];
      $c_name = $row['c_name'];
      $c_seq = $row['c_seq'];
      $b_id   = explode(",",$row['b_id']);
      $is_active  = $row['is_active'];

    }
    if($action == "ADD"){
     $btn = "Save changes";
    }
    ?>
    <input type="hidden" name="action" value="<?=$action?>">
    <input type="hidden" name="id" value="<?=@$id?>">
    <input type="hidden" name="c_id" value="<?=@$id?>">
    <div class="modal-body">
      <div class="row">
        <div class="col-md-10">
          <div class="form-group">
            <label>CATEGORY NAME</label>
            <input value="<?=@$c_name?>" name="c_name" type="text" class="form-control" placeholder="CATEGORY NAME" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>SEQ</label>
            <input value="<?=@$c_seq?>" min="1" name="c_seq" type="number" class="form-control" placeholder="SEQ" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>BRAND</label>
            <select class="form-control select2" name="b_id[]"  required>
              <?php
                $sqlc = "SELECT * FROM brand";
                $queryc = DbQuery($sqlc,null);
                $rowc   = json_decode($queryc, true);
                if($rowc['dataCount'] > 0){
                  foreach ($rowc['data'] as $valuec) {
              ?>
              <option value="<?=$valuec['b_id']?>" <?=in_array($valuec['b_id'], $b_id)?"selected":""?>><?=$valuec['b_name']?></option>
              <?php }} ?>
            </select>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>STATUS</label>
            <select class="form-control" name="is_active" required>
              <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
              <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
    </div>

    <script type="text/javascript">
      $('.select2').select2({
        placeholder: "BRAND"
      });
    </script>
