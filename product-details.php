<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>MTA Service - Index</title>

  <?php include('inc/css.php'); ?>
  <style>
     h3 {
       font-size: 1.5rem;
     }
  </style>
</head>

<body onload="getButton()">

  <?php
    include('inc/connect.php');
    include('inc/mainFunc.php');
    include('inc/nav2.php');

    $id= $_POST['id'];
  ?>

  <main id="main">
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="index.php">Home</a></li>
          <li>Product Details</li>
        </ol>
        <h2>Product Details</h2>

      </div>
    </section><!-- End Breadcrumbs -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <div class="row">
         <div class="col-md-7">
           <?php
           $p_id = isset($_POST['id'])?$_POST['id']:"";
           if($p_id == ""){
             exit("<script>window.location='index.php'</script>");
           }
           $sql = "SELECT * FROM product WHERE p_id = '$p_id'";
           $query = DbQuery($sql,null);
           $json   = json_decode($query, true)['data'][0];
           // $new_img = $json['p_img'];
           if(!empty($json['p_imgSlide'])){
             $new_img = $json['p_imgSlide'];
           }
           $c_id   = $json['c_id'];
           $b_id   = $json['b_id'];
           $cid    = "";

           if($c_id != ""){
              $cidArr = explode(",",$c_id);
              $numRam = array_rand($cidArr,1);
              $cid    = $cidArr[$numRam];
           }


           $new_img = explode("[:]",$new_img);

           ?>
             <div id="custCarousel" class="carousel slide" data-ride="carousel" align="center">
                 <!-- slides -->
                 <div class="carousel-inner">
                     <?php
                      foreach ($new_img as $key => $value) {
                     ?>
                     <div class="carousel-item <?=$key==0?"active":""?>"> <img src="image/product/<?=$value?>"> </div>
                     <?php } ?>
                 </div> <!-- Left right -->
                 <a class="carousel-control-prev" href="#custCarousel" data-slide="prev"> <i class="fa fa-chevron-left" style="font-size:20px;color:#aaa"></i> </a>
                 <a class="carousel-control-next" href="#custCarousel" data-slide="next"> <i class="fa fa-chevron-right" style="font-size:20px;color:#aaa"></i> </a> <!-- Thumbnails -->
                 <ol class="carousel-indicators list-inline">
                     <?php
                      foreach ($new_img as $key => $value) {
                     ?>
                     <li class="list-inline-item <?=$key==0?"active":""?>">
                       <a id="carousel-selector-<?=$key?>" class="<?=$key==0?"selected":""?>" data-slide-to="<?=$key?>" data-target="#custCarousel">
                         <img src="image/product/<?=$value?>" class="img-fluid">
                       </a>
                     </li>
                     <?php } ?>
                 </ol>
             </div>
         </div>
         <div class="col-md-5 pdetail">
           <h3 style=""><b><?=$json['p_name'] ?></b></h3>
           <hr>
           <div>
             <?= $json['p_detail'] ?>
           </div>
         </div>
        </div>
        <?php if($json['p_detail2'] != "") { ?>
        <br>
        <div>
          <h3><b>รายละเอียด</b></h3>
            <hr>
          <?= $json['p_detail2'] ?>
        </div>
      <?php } ?>
        <section id="product" class="portfolio">
          <h4><b>สินค้าที่เกี่ยวข้อง</b><h4>
          <hr>
          <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
            <?php

            $sqls   = "SELECT p.*, b.b_code , b.b_name
                       FROM product p , brand b
                       WHERE p.p_id not in ('$p_id') and FIND_IN_SET($cid,p.c_id)>0
                       and p.is_active = 'Y' and p.b_id = b.b_id
                       ORDER BY p.p_seq ASC
                       limit 0,5";
            $querys = DbQuery($sqls,null);
            $json       = json_decode($querys, true);
            $row        = $json['data'];
            $dataCount  = $json['dataCount'];

            if($dataCount == 0){
              $sqls   = "SELECT p.*, b.b_code , b.b_name
                         FROM product p , brand b
                         WHERE p.p_id not in ('$p_id') and p.b_id = $b_id
                         and p.is_active = 'Y' and p.b_id = b.b_id
                         ORDER BY p.p_seq ASC
                         limit 0,5";

              $querys = DbQuery($sqls,null);
              $json       = json_decode($querys, true);
              $row        = $json['data'];
              $dataCount  = $json['dataCount'];
            }

            for($i = 0; $i < $dataCount; $i++){
              $b_name   =   $row[$i]['b_name'];
              $b_code   =   $row[$i]['b_code'];
              $p_id     =   $row[$i]['p_id'];
              $p_name   =   $row[$i]['p_name'];
              $is_show  =   $row[$i]['is_show'];
              $p_img    =   "image/product/".$row[$i]['p_img'];

              $filter   =  "filter-".$b_code;
            ?>
            <div class="col-lg-2 col-md-6 portfolio-item <?=$filter;?>" onclick="postURL('product-details.php?id=<?=$p_id; ?>', null)">
              <div class="portfolio-img"><img src="<?= $p_img ?>" class="img-fluid" alt=""></div>
              <div class="portfolio-info">
                <p style="font-size:16px"><?=$p_name ?></p>
                <hr style="border-top: 1px solid #fff;">
                <div class="row">
                  <div class="col-8">
                      <p><?=$b_name ?></p>
                  </div>
                  <div class="col-4">
                    <a href="<?= $p_img ?>" data-gall="portfolioGallery" class="venobox preview-link" title="ProRox"><i class="bx bx-image"></i></a>
                    <a onclick="postURL('product-details.php?id=<?=$p_id; ?>', null)" class="details-link" title="More Details"><i class="bx bx-info-square"></i></a>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          </div>
        </section>
      </div>

  </main><!-- End #main -->

  <?php
    include('inc/footer.php');
    include('inc/js.php');
  ?>

</body>

</html>
