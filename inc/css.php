<meta content="3M mta firebarrier Rockwool mta-firebarrier Maxflex" name="descriptison">
<meta content="3M, mta, firebarrier, Rockwool, mta-firebarrier, Maxflex" name="keywords">

<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/favicon.png" rel="mta-firebarrier">

<!-- Google Fonts -->
<link href="assets/css/fonts.css" rel="stylesheet">
<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/banner.css" rel="stylesheet">
<link href="assets/fontawesome/css/all.css" rel="stylesheet">
<link href="assets/css/main.css" rel="stylesheet">
<link href="assets/css/smoke.css" rel="stylesheet">

<link href="assets/css/carousel.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
