<!-- ======= Footer ======= -->
<footer id="footer">

  <div class="footer-newsletter" style="display:none;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <h4>Join Our Newsletter</h4>
          <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
          <form action="" method="post">
            <input type="email" name="email"><input type="submit" value="Subscribe">
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-top" style="background: #f3f5fa;">
    <div class="container">
      <div class="row">

        <div class="col-lg-3 col-md-6 footer-contact">
          <h3>MTA</h3>
          <p>
            189/395 หมู่ 1 <br>
            ตำบลบางรักพัฒนา อำเภอบางบัวทอง<br>
            นนทบุรี 11110<br><br>
            <strong>Phone:</strong> 0953656499<br>
            <strong>Email:</strong> mta.service@hotmail.com<br>
          </p>
        </div>

        <div class="col-lg-3 col-md-6 footer-links">
          <h4>Useful Links</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="index.php">หน้าหลัก</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#product">สินค้า</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="product.php">รายการสินค้า</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#services">บริการ</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#about">เกี่ยวกับเรา</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#contact">ติดต่อเรา</a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 footer-links">
          <h4>ยี่ห้อสินค้า</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="product.php">3M</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="product.php">Rockwool</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="product.php">Maxflex</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="product.php">Other</a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 footer-links" style="display:none">
          <h4>Our Social Networks</h4>
          <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
          <div class="social-links mt-3">
            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="container footer-bottom clearfix">
    <div class="copyright">
      &copy; Copyright <strong><span>MTA Service</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
      Designed by <a href="">Onesittichok Co,.ltd</a>
    </div>
  </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
<div id="preloader"></div>
