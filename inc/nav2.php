<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
  <div class="d-flex align-items-center navmta">
    <div class="container nav-text">
       <p class="fontArial alignleft">www.mta-firebarrier.com</p>
       <p class="alignright"><i class="fa fa-mobile-alt"></i> โทร 095-365-6499 , 081-701-5232</p>
    </div>
  </div>
  <div class="container d-flex align-items-center" >

    <h1 class="logo mr-auto">
      <a href="index.php">
        <img src="assets/img/logo_mta.png">
      </a>
    </h1>
    <!-- Uncomment below if you prefer to use an image logo -->
    <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

    <nav class="nav-menu d-none d-lg-block">
      <ul>
        <li><a href="index.php"><i class="fas fa-home icon-h"></i>&nbsp;หน้าหลัก</a></li>
        <li class="active"><a href="product.php"><i class="fas fa-shopping-bag icon-h"></i>&nbsp;รายการสินค้า</a></li>
      </ul>
    </nav><!-- .nav-menu -->

    <!-- <a href="#about" class="get-started-btn scrollto">Get Started</a> -->
  </div>
</header><!-- End Header -->
