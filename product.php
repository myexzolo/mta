<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>MTA Service - Product</title>
  <?php include('inc/css.php'); ?>
  <link rel="stylesheet" href="css/munu.css">
</head>

<body onload="getButton()">

  <?php
    include('inc/connect.php');
    include('inc/mainFunc.php');
    include('inc/nav2.php');
    // include('page/banner.php');
  ?>

  <main id="main">

    <?php
        include('page/product_cat.php');
    ?>


  </main><!-- End #main -->

  <?php
    include('inc/footer.php');
    include('inc/js.php');
  ?>

</body>

</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });

        catagory("")

    });

    function catagory(id)
    {
      $(".active").each(function(){
        $(this).removeClass("active");
      });
      if(id != ""){
        $( "#"+id ).addClass("active");
      }
      $.post( "page/product_cats.php",{c_id:id})
      .done(function( data ) {
         $("#show-product").html(data);
      });
    }
</script>
