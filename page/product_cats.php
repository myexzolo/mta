<div class="row" data-aos="fade-up" data-aos-delay="200" style="margin-top:20px;">
<?php
include('../inc/connect.php');
include('../inc/mainFunc.php');
$c_id = isset($_POST['c_id'])?$_POST['c_id']:"";
$con = "";
$seq = "";
if($c_id == ""){
  $con = " and is_show = 'Y' ";
  $seq = "p.p_seq_show,";
}else{
  $con = " and find_in_set('$c_id',c_id) <> 0 ";
}

$sql = "SELECT p.*, b.b_code , b.b_name FROM product p , brand b
        WHERE p.is_active = 'Y' and p.b_id = b.b_id $con ORDER BY $seq p.p_seq ASC";
$query = DbQuery($sql,null);
$json   = json_decode($query, true);
$dataCount = $json['dataCount'];
$rows      = $json['data'];
//echo $sql;

for($i=0; $i < $dataCount ;$i++)
{
  $b_name   =   $rows[$i]['b_name'];
  $b_code   =   $rows[$i]['b_code'];
  $p_id     =   $rows[$i]['p_id'];
  $p_name   =   $rows[$i]['p_name'];
  $is_show  =   $rows[$i]['is_show'];
  $p_img    =   "image/product/".$rows[$i]['p_img'];
?>
<div class="col-lg-2 col-md-6 product-item" onclick="postURL('product-details.php?id=<?=$p_id; ?>', null)">
  <div class="img-item"><img src="<?= $p_img ?>" class="img-fluid" alt="" style="margin-top:10px;"></div>
  <p style="font-size:16px"><?=$p_name ?></p>
</div>
<?php
}
?>
</div>
