<!-- ======= Product Section ======= -->
<section id="product" class="portfolio pdd-130">
  <div class="container" data-aos="fade-up">
    <div class="section-title" style="padding-bottom: 10px;">
      <h2>สินค้า</h2>
    </div>
    <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100" style="padding-left:0px">
      <li data-filter=".filter-show" class="filter-active">All</li>
    <?php
    $sql = "SELECT * FROM brand WHERE is_active != 'D' ORDER BY b_seq ASC";
    $query = DbQuery($sql,null);
    $json   = json_decode($query, true);
    if($json['dataCount'] > 0){
      foreach ($json['data'] as $key => $value){
    ?>
      <li data-filter=".filter-<?=$value['b_code']?>"><?=$value['b_name']?></li>
    <?php }} ?>
    </ul>

    <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
      <?php
      $sql = "SELECT p.*, b.b_code , b.b_name FROM product p , brand b
              WHERE p.is_active = 'Y' and p.b_id = b.b_id ORDER BY p.p_seq ASC";
      $query = DbQuery($sql,null);
      $json   = json_decode($query, true);
      $dataCount = $json['dataCount'];
      $rows      = $json['data'];

      for($i=0; $i < $dataCount ;$i++)
      {
        $filterShow = "filter-hide";

        $b_name   =   $rows[$i]['b_name'];
        $b_code   =   $rows[$i]['b_code'];
        $p_id     =   $rows[$i]['p_id'];
        $p_name   =   $rows[$i]['p_name'];
        $is_show  =   $rows[$i]['is_show'];
        $p_img    =   "image/product/".$rows[$i]['p_img'];

        $filter   =  "filter-".$b_code;

        if($is_show == "Y"){
          $filterShow = "filter-show";
        }
      ?>
      <div class="col-lg-3 col-md-6 portfolio-item <?=$filter." ".$filterShow; ?>" onclick="postURL('product-details.php?id=<?=$p_id; ?>', null)">
        <div class="portfolio-img"><img src="<?= $p_img ?>" class="img-fluid" alt=""></div>
        <div class="portfolio-info">
          <p style="font-size:16px"><?=$p_name ?></p>
          <hr style="border-top: 1px solid #fff;">
          <div class="row">
            <div class="col-8">
                <p><?=$b_name ?></p>
            </div>
            <div class="col-4">
              <a href="<?= $p_img ?>" data-gall="portfolioGallery" class="venobox preview-link" title="ProRox"><i class="bx bx-image"></i></a>
              <a onclick="postURL('product-details.php?id=<?=$p_id; ?>', null)" class="details-link" title="More Details"><i class="bx bx-info-square"></i></a>
            </div>
          </div>
        </div>
      </div>
      <?php
      }
      ?>
    </div>
  </div>
</section><!-- End Portfolio Section -->
