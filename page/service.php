<!-- ======= Services Section ======= -->
<section id="services" class="services section-bg">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2>บริการ</h2>
      <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
    </div>

    <div class="row">
      <div class="col-xl-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100" style="margin-bottom:20px;">
        <div class="icon-box">
          <img src="assets/img/service/1-11.jpg" class="img-fluid" alt="" style="padding-bottom:10px;">
          <h4><a href="">ประเมินราคา</a></h4>
          <p>เรามุ่งมั่นที่จะตอบสนองความต้องการของลูกค้า ในมาตรฐานของสินค้าและความคุ้มค่าของราคา</p>
        </div>
      </div>

      <div class="col-xl-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="200" style="margin-bottom:20px;">
        <div class="icon-box">
          <img src="assets/img/service/3m.png" class="img-fluid" alt="" style="padding-bottom:10px;">
          <h4><a href="">บริการติดตั้ง</a></h4>
          <p>เราให้บริการติดตั้งและตรวจเช็คความถูกต้อง ตรวจเช็คหน้างาน ระบบป้องกันไฟและควันลาม (Fire Barrier System) ในทุกช่องชาร์ปหรือไม่ว่าช่องเปิดทุกชนิด ที่ผ่านพื้นและผนัง  รอยต่อโครงสร้าง ในงานระบบไฟฟ้า ระบบสุขาภิบาล ระบบปรับอากาศ และรวมไปถึงระบบดับเพลิง ด้วยวัสดุป้องกันไฟลาม มาตรฐานสินค้า 3M</p>
        </div>
      </div>


      <div class="col-xl-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="300" style="margin-bottom:20px;">
        <div class="icon-box">
          <img src="assets/img/service/3-6.jpg" class="img-fluid" alt="" style="padding-bottom:10px;">
          <h4><a href="">บริการหลังการขาย</a></h4>
          <p>ทีมงานที่เชี่ยวชาญของเราพร้อมให้คำปรึกษาดูแลทั้งด้านงานขายและติดตั้ง ระบบป้องกันไฟและควันลาม (Fire Barrier System) ตามมาตรฐาน 3M เพื่อให้เหมาะสมกับความต้องการ และเกิดประโยชน์สูงสุดสำหรับหน่วยงานหรืออาคารที่ท่านดูแล</p>
        </div>
      </div>


    </div>

  </div>
</section><!-- End Services Section -->
