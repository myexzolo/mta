<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center ">
  <div class="banner has-hover">
    <div class="banner-img"></div>
    <div class="banner-container">
      <div class="contain">
        <p class="banner-text"><b>3M Fire Protection</b><p>
        <p class="banner-text2">Confidence Even Under Fire</p>
      </div>
      <img src="assets/img/product/22.png" class="img-banner" >
    </div>
  </div>
  <div>
    <video class="video-bg fill visible" preload="" playsinline="" autoplay="" muted="" loop="">
		<source src="assets/vdo/3m_m.mp4" type="video/mp4">
    </video>
    <div class="overlay"></div>
    <div class="text-banner">
      3M Fire Protection
      <br>
      Confidence Even Under Fire
    </div>
    <!-- <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1>Better Solutions For Your Business</h1>
        <h2>We are team of talanted designers making websites with Bootstrap</h2>
        <div class="d-lg-flex">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox btn-watch-video" data-vbtype="video" data-autoplay="true"> Watch Video <i class="icofont-play-alt-2"></i></a>
        </div>
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="assets/img/hero-img.png" class="img-fluid animated" alt="">
      </div> -->
  </div>
</section><!-- End Hero -->
<!-- ======= Cliens Section ======= -->
<section id="cliens" class="cliens section-bg">
  <div class="container">
    <div class="row" data-aos="zoom-in">
      <div class="col-lg-4 col-md-4 col-4 d-flex align-items-center justify-content-center">
        <img src="assets/img/clients/3m.png" class="img-fluid" alt="">
      </div>

      <div class="col-lg-4 col-md-4 col-4 d-flex align-items-center justify-content-center">
        <img src="assets/img/clients/rockwool.png" class="img-fluid" alt="">
      </div>

      <div class="col-lg-4 col-md-4 col-4 d-flex align-items-center justify-content-center">
        <img src="assets/img/clients/maxflex.png" class="img-fluid" alt="">
      </div>


    </div>

  </div>
</section><!-- End Cliens Section -->
