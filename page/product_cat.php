<section id="product" class="portfolio catagory" style="padding:0;">
  <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <ul class="list-unstyled components">
                <p style="border-bottom: 1px solid #ccc;margin-bottom: 0px;font-size:1.3em">Category</p>
                <?php
                  $sql = "SELECT * FROM brand WHERE is_active = 'Y' ORDER BY b_seq";
                  $query = DbQuery($sql,null);
                  $json   = json_decode($query, true);
                  $dataCount = $json['dataCount'];
                  $rows      = $json['data'];

                  for($i=0; $i < $dataCount ;$i++)
                  {
                    $b_id     =   $rows[$i]['b_id'];
                    $b_code   =   $rows[$i]['b_code'];
                    $b_name   =   $rows[$i]['b_name'];

                    $sqlc     = "SELECT * FROM category WHERE is_active = 'Y' and b_id = '$b_id' ORDER BY c_seq";
                    $queryc   = DbQuery($sqlc,null);
                    $jsonc    = json_decode($queryc, true);
                    $dataCountc = $jsonc['dataCount'];
                    $rowsc      = $jsonc['data'];

                    if($dataCountc > 1)
                    {
                  ?>
                      <li>
                          <a href="#<?=$b_code?>" data-toggle="collapse" aria-expanded="false"><?= $b_name ?></a>
                          <ul class="collapse list-unstyled" id="<?=$b_code?>">
                           <?php
                            for($x=0; $x < $dataCountc ;$x++)
                            {
                              $c_id     =   $rowsc[$x]['c_id'];
                              $c_name   =   $rowsc[$x]['c_name'];
                            ?>
                              <li style="border-bottom: 1px solid #ccc;" id="<?=$c_id?>"><a onclick="catagory(<?=$c_id?>)"><?=$c_name?></a></li>
                            <?php }
                            ?>
                          </ul>
                      </li>
                  <?php
                    }else{
                      $c_id = "";
                      $c_name = "";
                      if($dataCountc == 1)
                      {
                        $c_id     =   $rowsc[0]['c_id'];
                        $c_name   =   $rowsc[0]['c_name'];
                      }
                  ?>
                  <li id="<?=$c_id?>">
                      <a onclick="catagory(<?=$c_id?>)"><?= $b_name ?></a>
                  </li>
                  <?php
                    }
                  }
                ?>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" class="menu" style="width:100%">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" style="background-color:#14007d;color:#fff;" class="btn btn-flat navbar-btn">
                            <i class="fa fa-align-left"></i>
                            <span>หมวดหมู่สินค้า</span>
                        </button>
                    </div>

                    <!-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                        </ul>
                    </div> -->
                    <div id="show-product" style="width:100%"></div>
                </div>
            </nav>

        </div>
    </div>
</section>
